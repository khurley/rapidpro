#/bin/bash

#
# Windows Builds
#
if [ ! -d "../ThirdParty/GLFW" ]; then
	mkdir -p ../ThirdParty/GLFW
fi

cd ../ThirdParty/GLFW

if [ ! -f "README.md" ]; then
	 git clone https://github.com/glfw/glfw.git .
fi

if [! -d "lib/" ]; then
	mkdir -p lib
fi

echo *** Generating GLFW Win Builds ***
if [! -d "builds/Win32/Debug" ]; then
mkdir -p builds/Win32/Debug
fi

if [! -d "builds/Win32/Release" ]; then
mkdir -p builds/Win32/Debug
fi

if [! -d "builds/Win64/Debug" ]; then
mkdir -p builds/Win32/Debug
fi

if [! -d "builds/Win64/Relase" ]; then
mkdir -p builds/Win32/Debug
fi

cd builds/Win32/Debug
cmake -G"NMake Makefiles" ..//..//..// -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY="..//..//..//..//lib//Win32//Debug" -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF

cd builds/Win32/Debug
cmake -G"NMake Makefiles" ..//..//..// -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY="..//..//..//..//lib//Win32//Debug" -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF

cd builds/Win32/Debug
cmake -G"NMake Makefiles" ..//..//..// -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY="..//..//..//..//lib//Win32//Debug" -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF

cd builds/Win32/Debug
cmake -G"NMake Makefiles" ..//..//..// -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY="..//..//..//..//lib//Win32//Debug" -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF

echo *** Building Win32 Debug GLFW library ***
NMake.exe
if [! -f ../../../lib/Win32/Debug/glfw3.lib ]; then
	echo --- Build Failed check output ---
	exit 1;
fi

echo *** Building Win32 Release GLFW library ***
msbuild.exe GLFW.sln //p:Configuration=Release //p:Platform="Win32"
if [! -f ../../lib/Win32/Release/glfw3.lib ]; then
	echo --- Build Failed check output ---
	exit 1;
fi

echo *** Generating GLFW Win64 Build ***
cd ../Win64
cmake ..//..// -DCMAKE_ARCHIVE_OUTPUT_DIRECTORY="..//..//..//lib//Win64" -DGLFW_BUILD_EXAMPLES=OFF -DGLFW_BUILD_TESTS=OFF -DGLFW_BUILD_DOCS=OFF

echo *** Building Win32 Debug GLFW library ***
msbuild.exe GLFW.sln //p:Configuration=Debug //p:Platform="Win32"
if [! -f ../../lib/Win32/Debug/glfw3.lib ]; then
	echo --- Build Failed check output ---
	exit 1;
fi

echo *** Building Win32 Release GLFW library ***
msbuild.exe GLFW.sln //p:Configuration=Release //p:Platform="Win32"
if [! -f ../../lib/Win32/Release/glfw3.lib ]; then
	echo --- Build Failed check output ---
	exit 1;
fi


